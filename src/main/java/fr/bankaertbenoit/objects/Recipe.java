package fr.bankaertbenoit.objects;

import java.util.List;
import java.util.Map;

/**
 * @author Bankaert Benoit
 * @version 0.1.0
 * This class represente an recipe object
 */
public class Recipe {

    private int id;
    private List<String> step;
    private int time;
    private Map<Ingredient,Integer> ingredients;
    private String name;

    /**
     * Instanciate a recipe with parameters
     * @param i is the id of the recipe ( has to be unique )
     * @param s is the list of step
     * @param t is the time needed for the recipe
     * @param is is the list of ingredients associate with the quantity needed
     * @param n is the name of the recipe
     */
    public Recipe(int i,List<String> s, int t, Map<Ingredient,Integer> is, String n){
        this.id = i;
        this.step = s;
        this.time = t;
        this.ingredients = is;
        this.name = n;
    }

    public String getStepWithFormats(){
        String steps = "";
        for(int i = 0; i < this.step.size(); i++){
           steps += "Etape "+(i+1)+"\n"+this.step.get(i)+"\n";
        }
        return steps;
    }

    /**
     * @return the id of the recipe
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the list of step of the recipe
     */
    public List<String> getStep() {
        return step;
    }

    public void setStep(List<String> step) {
        this.step = step;
    }

    /**
     * @return the time nedded for the recipe in minute
     */
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    /**
     * @return the list of ingredients associate with the quantity
     */
    public Map<Ingredient, Integer> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<Ingredient, Integer> ingredients) {
        this.ingredients = ingredients;
    }

    /**
     * @return the name of the recipe
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
