package fr.bankaertbenoit.factory;

import fr.bankaertbenoit.objects.Ingredient;
import fr.bankaertbenoit.objects.Recipe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bankaert Benoit
 * @version 0.1.0
 * This factory is here to simplify the creating of recipe
 */
public class RecipeFactory {

    /**
     * Create a Recipe with the given parameters
     * @param id is the id of the recipe
     * @param steps is the list of steps of the recipe
     * @param time is the time needed for the recipe
     * @param ingredients is the list of ingredients associate to their quantity needed for the recipe
     * @param name is the full name of the recipe
     * @return a recipe with the given parameters
     */
    public static Recipe createRecipe(int id, List<String> steps, int time, Map<Ingredient,Integer> ingredients, String name){
        return new Recipe(id,steps,time,ingredients,name);
    }
}
