package fr.bankaertbenoit.factory;

import fr.bankaertbenoit.objects.Ingredient;
import fr.bankaertbenoit.objects.Recipe;

import java.util.List;
import java.util.Map;

/**
 * @author Bankaert Benoit
 * @version 0.1.0
 * This class is here to help the creation of ingredients
 */
public class IngredientFactory {

    /**
     * Create on ingredient with the given parameters
     * @param id is the id of the ingredient
     * @param name is the full name of the ingredient
     * @return an ingredient with the given parameters
     */
    public static Ingredient createRecipe(String id,String name){
        return new Ingredient(id,name);
    }
}
