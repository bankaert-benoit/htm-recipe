package fr.bankaertbenoit.objects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IngredientTest {

    @Test
    public void createIngredientTest(){
        String id = "tomate";
        String name = "tomate";
        Ingredient i = new Ingredient(id,name);
        assertEquals(id,i.getId());
        assertEquals(name,i.getName());
    }
}
